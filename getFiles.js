const superagent = require("superagent")
const fs = require('fs')
const path = require('path')

const beautify = require('js-beautify').js

const stylesheet = /stylesheet" href="(.*?\.css)/g
const script = /script src="(\/assets\/.{20}\.js)/g
const loaderRegex = /.\..=.=>\((\{.*?\})\[./gs
const jsonFix = /([0-9a-z]*):/g
const canary = 'https://canary.discord.com/app'
const assetHost = 'https://canary.discord.com'

let searchTarget;
let noLocales = false;
if (process.argv[2].startsWith('-target:') && process.argv[2].length >= 9) {
  searchTarget = process.argv.join(' ').split('-target:')[1]
  if(searchTarget.includes('-nl')){
    searchTarget = searchTarget.replace(/ ?-nl/, '')
    noLocales = true
    console.log(`Ignoring localization files.`)
  }
  console.log(`Searching for \x1b[36m${searchTarget}\x1b[0m`)
} else {
  console.log('Downloading all files.')
}

async function saveFile(content, scr) {
    fs.writeFileSync(`./files/${scr}.js`, beautify(content, { indent_size: 2, space_in_empty_parent: true }))
    process.stdout.clearLine(0);
    process.stdout.cursorTo(0);
    console.log(`Found target in \x1b[36m${scr}\x1b[0m.`)
}

async function download() {
  let site;
  try {
    site = await superagent.get(canary)
    const match = site.text.match(script)
    scriptData = await superagent.get(`${assetHost}${match[0].replace(`script src="`, '')}`)
    scripts = loaderRegex.exec(scriptData.body.toString().replace(/\n/g, ''))
    scriptData = scripts[1].toString().replace(jsonFix, "\"$1\":")
    scripts = JSON.parse(scriptData)
    fs.readdir('./files/', (err, files) => {
      if (err) throw err;

      for (const file of files) {
        fs.unlink(path.join('./files/', file), err => {
          if (err) throw err;
        });
      }
    });
    let projektor = {}

    if (!searchTarget) {
      let keys = ["chunkloader","classes","react","client"]
      let i = 0;
      for (const root of match) {
        scriptc = await superagent.get(`${assetHost}${root.replace(`script src="`, '')}`)
        fs.writeFileSync(`./files/${keys[i]}.js`, beautify(scriptc.body.toString(), { indent_size: 2, space_in_empty_parent: true }), 'utf8')
        console.log(`Downloaded \x1b[36m${keys[i]}\x1b[0m.`)
        i++;
      };
      const matches = stylesheet.exec(site.text)
      style = await superagent.get(`${assetHost}${matches[1]}`)
      fs.writeFileSync('./files/app.css', style.text, 'utf8')
      console.log(`Downloaded \x1b[36mcss\x1b[0m.`)
      fs.writeFileSync('./files/client.html', site.text.replace(`RELEASE_CHANNEL: 'canary',`, `RELEASE_CHANNEL: 'staging',`).replace(/integrity="(.*?)"/g, ''), 'utf8')
      fs.writeFileSync(`./projektor.json`, JSON.stringify(projektor))
    }
    //add root scripts
    let i = 0;
    for (const root of match) {
      scripts[i] = root.replace(/script src="\/assets\/(.*?).js/, '$1');
      i++;
    };
    let expFile = [];
    let processed = 0;
    if (searchTarget) {
      Object.values(scripts).forEach(async function (scr, i) {
        try{
          data = await superagent.get(`${assetHost}/assets/${scr}.js`)
          if (data.body.toString().includes(searchTarget)) {
            if(noLocales){
              if(data.body.toString().includes(`.exports=JSON.parse('{"DISCORD_DESC`)){
                process.stdout.clearLine(0);
                process.stdout.cursorTo(0);
                console.log(`Found target in \x1b[36m${scr}\x1b[0m (locale, ignoring).`)
              } else {
                expFile.push(scr)
                saveFile(data.body.toString(), scr)
                projektor[scr] = scr
              }
            } else {
              expFile.push(scr)
              saveFile(data.body.toString(), scr)
              projektor[scr] = scr
            }
          }
        }catch(e){
          process.stdout.clearLine(0);
          process.stdout.cursorTo(0);
          console.log(`Unable to process chunk \u001b[31;1m${scr}\x1b[0m.`)
        }
        processed++
        
        process.stdout.cursorTo(0);
        process.stdout.write(`Progress: ${processed}/${Object.values(scripts).length}`);
        if (processed == Object.values(scripts).length) {
          fs.writeFileSync('./files/client.html', site.text.replace(`RELEASE_CHANNEL: 'canary',`, `RELEASE_CHANNEL: 'staging',`).replace(/integrity="(.*?)"/g, ''), 'utf8')
          if (expFile.length == 0) {
            process.stdout.clearLine(0);
            process.stdout.cursorTo(0);
            console.log(`No results found.`)
            process.exit();
          }
          if (expFile.length >= 2) {
            process.stdout.clearLine(0);
            process.stdout.cursorTo(0);
            console.log(`Found \x1b[36m${expFile.length} results\x1b[0m.`)
          } else {
            process.stdout.clearLine(0);
            process.stdout.cursorTo(0);
            console.log(`Found \x1b[36m${expFile.length} result\x1b[0m.`)
          }
          fs.writeFileSync(`./projektor.json`, JSON.stringify(projektor))
        }
      })
    }
  } catch (e) {
    process.stdout.clearLine(0);
    process.stdout.cursorTo(0);
    console.log('[Error] Failed to download discord data:')
    console.log(e)
  }
}

(async () => {
  await download()
})();
