const express = require('express');
const superagent = require('superagent');
const fs = require('fs');

const config = require('./projektor.json')

const app = express();

const assetHost = 'https://canary.discord.com/assets/';

const port = 3333;

//Launch the modified app
async function launch(req, res){
  index = fs.readFileSync('./files/client.html', 'utf-8');
  res.setHeader('Content-Type', 'text/html');
  return res.send(index);
}

//Asset Proxy
app.get('/assets/:asset', async function(req, res){
  if(req.params.asset.endsWith('.map')){ //source maps wont work anyways, no need to spam proxy discord for them
    return res.status(400).send('')
  }
  if(config[req.params.asset.replace('.js','')]){
    file = fs.readFileSync(`./files/${config[req.params.asset.replace('.js','')]}.js`, 'utf-8')
    res.setHeader('Content-Type', 'application/javascript')
    return res.status(200).send(file)
  }
  try{
    const site = await superagent.get(`${assetHost}${req.params.asset}`);
    res.setHeader('Content-Type', site.headers['content-type']);
    if(req.params.asset.endsWith('css')){ return res.send(site.text); };
    return res.send(site.body);
  } catch(e){
    res.status(404).send('Not Found.');
  };
})

app.get('*', async function(req, res){
  launch(req, res)
})

app.listen(port, () => {
  console.log(`[PROJEKTOR] projektor live under \x1b[36m\x1b[4mhttp://localhost:${port}\x1b[0m`);
});