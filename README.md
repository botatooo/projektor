# projektor

discord client research tool

## usage

```diff
- it is advised you only use this if you know what you are doing. idiot mistakes can easily get your account disabled by antispam -
```

run `node getFiles` to download the core client files, as well as the latest app html file.

use `node getFiles -t:<search string>` to find something specific, some examples are
```
node getFiles -target:Messages.APP_DIRECTORY
node getFiles -target:fetchSoundsForGuild
```

### downloaded files

all js files with which contain the query will be beautified and placed under `/files/`.

## projektor runner configuration

the projektor runner serves the client under `http://localhost:3333`. the `projektor.json` file contains a list of files which will be served locally, using the format `"asset hash": "local file name"`. an example configuration would be

*Canary 112560 (ac83324)*
```json
{"79c80b8e658a994c8fdc":"client","4a404d84f4baf29ede02":"webpack","ae8017d8f9a7beea125c":"classes"}
```

launch the projektor runner with `node projektor` and visit `http://localhost:3333` in your browser.
